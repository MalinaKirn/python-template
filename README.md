Python Template
=======================
This is a GitHub project with template files to get you started with a new Python project. It provides the following features:

1. Environment setup guide for Mac.
2. A setup.py file that will create a Python package for you that can be installed with pip or directly with python setuptools. The setup.py file should specify your package dependencies. The setup.py file also registers an executable, if desired.
3. A requirements.txt file that can be used to ensure repeatable builds, especially important for packages intended as libraries.
2. Unit tests with nose, including a test for PEP8 style guidelines.
3. An apidocs configuration file that can be used to automatically generate HTML API documentation, if desired. 
4. A template README.md to place in your project, if desired.

Note that this template is designed for Python 2.7, though can likely be trivially upgraded to Python 3 syntax.

If your environment is already set up with python and pip, feel free to skip the next section. The section *"Using this template"* gives an introduction on minimum steps needed to use the template; the rest of the sections provide additional information. Finally, there is a template README section below titled *"Documentation you may wish to put in your own project"* that you can keep for your new package, or discard. This section describes how users and developers of your package can install your package, run tests on your package, and create API docs for your package.


Getting your environment set up
-------------------------------
The below instructions are for a Mac. Although Python programming is certainly possible on a Windows, instructions to get the environment set up are not included here. The below installs brew, installs python and pip using brew, and installs virtualenv and virtualenvwrapper using pip.

1. Install [Brew](http://brew.sh/)
2. Add to your ~/.bashrc or ~/.bash_profile: `export PATH="/usr/local/bin:/usr/local/sbin:$PATH"`
2. Run `brew update`
2. Run `brew doctor` to see if any serious issues need repair, fix any significant issues
	- most .dll warnings can be ignored
	- `PATH` issues are significant, fix them
	- Ownership issues are often significant and most of the time you should fix them, especially in El Capitan. If you upgraded from an earlier release of Mac OS to El Capitan and are having problems with brew, [check out this post](http://digitizor.com/fix-homebrew-permissions-osx-el-capitan/).
3. `brew install python@2`
4. `pip2 install --upgrade pip`
4. `pip2 install virtualenvwrapper`
5. `mkdir ~/.virtualenvs`
5. Add to your ~/.bashrc or ~/.bash_profile

        export WORKON_HOME=$HOME/.virtualenvs
        source /usr/local/bin/virtualenvwrapper.sh

`virtualenvwrapper` is a set of convenience functions for use with virtualenv. virtualenv allows you to keep the dependencies of your various projects isolated. If one project needs pandas 0.16.0 and another needs 0.18.1, you can safely install both versions in separate environments without clobbering other environments. The most common commands you'll use are:

- `mkvirtualenv <env_name>` : make a virtual environment for a particular project or package you intend to install, including its dependencies; then switch to it
- `workon <env_name>` : switch to the specified virtual environment
- `deactivate` : deactivate your current virtual environment, falling back to the system default packages
- `rmvirtualenv <env_name>` : delete a virtual environment

After switching to a virtual environment, all commands to `pip install` or `python setup.py install` will install the package and its dependencies in the activated virtual environment, instead of into the system environment.

Since packages should state in their setup.py *all* of their required dependencies, you can usually create one virtualenv per application/main package you intend to use. That virtualenv should contain all the required dependencies to run the code in the package once you execute `pip install .`. This is a good test to discover if your own `setup.py` file contains all the required dependencies. If `pip install .` does not install both your package and all of your dependencies, you need to add them to your `setup.py` file.

Using this template
-------------------

This project is a template that creates a package with name `package_name`. When you'd like to create a package with a different name, let's say `your_name`, clone this repository into a new directory and execute the following inside that directory:

    rm -rf .git
    mv package_name your_name
    find . -type f -exec sed -i '' 's/package_name/your_name/g' {} +
    mkvirtualenv your_name
    # Edit setup.py & requirements.txt to specify author and dependencies
    pip install -r requirements.txt
    pip install -e .[tests]

Additionally, consider editing this README to use the template documentation below.

All source code should be placed in your renamed package_name directory (or any subdirectories inside package_name). All tests should be placed in subdirectories named `tests` and all resources should be placed in subdirectories named `resources`. Note that all source code and tests subdirectories should contain an \_\_init\_\_.py file (which could be empty), while all resources subdirectories should not.

Do not use relative imports in your code, rather, use imports relative to your new package. For example, suppose you have the following structure:

    package_name/__init__.py
    package_name/some_code.py
    package_name/new_subdirectory/__init__.py
    package_name/new_subdirectory/more_code.py

And suppose that the more_code.py module has a function named `function_a` and a class named `ClassA`. Then, to use `function_a` and `ClassA` in the some_code module, add the following to some_code.py:

    from package_name.new_subdirectory.more_code import ClassA
    from package_name.new_subdirectory import more_code
    
    ClassA(...)
    more_code.function_a(...)

Because you imported the class and fuction starting from the package name, the imports will always work once the package is installed on the system. Relative imports will break if users execute code from an unexpected location or don't install the source code, so it's strongly recommended you always specify the location of imports relative to the package.

***The below sections provide more information on the contents in this template, however, after executing the above, you're ready to go!***

Defining your Python package: setup.py
--------

setup.py uses python [setuptools](https://setuptools.readthedocs.io/en/latest/setuptools.html) to create your Python package. It states the location of the source files (located in the directory package_name or any subdirectory of package_name), as well as the location of non-python resource files (which must be located in any subdirectory named resources).

You'll probably want to edit setup.py to specify the author and email. You'll also want to add your dependencies to this file, typically in the `install_requires` argument.

The template setup.py includes numpy in the `install_requires` argument, which can be removed, if desired (simply set `install_requires` to an empty list). setup.py also provides recommended dependencies for running tests and creating apidocs. These dependencies are optional and can be modified or removed, if desired.

### Package resources

The setup.py file will include any and all files located in a non-tests subdirectory named resources in the python package (this is the function of the `package_data` argument).

Any non-test resources can (and should) be read by your code using pkg_resources. Do *not* use `os.path` directly to load package resources, always pass the relative path to `pkg_resources` to read the resource file. This will allow users of your code to load resources from your package no matter how the package is distributed (e.g., a wheel, which is the new default distribution format). Using `pkg_resources` in combination with `__name__` will allow you to specify the path relative to the code attempting to load the resource. For example, suppose you have the following file structure:

    package_name/some_code.py
    package_name/__init__.py
    package_name/resources/some_resource.txt

To use the some_resource.txt file in some_code.py, add the following lines to some_code.py:

    import pkg_resources, os

	resource_package = __name__
	resource_path = os.path.join('resources', 'some_resource.txt')
	resource_content_as_string = pkg_resources.resource_string(resource_package, resource_path)

Tests are excluded in the package created by setuptools (as tests are not required for the code to function). Consequently, resources needed in tests should be loaded from the relative path in which tests are executed (this top-level directory). An example of doing this is available in the file package_name/tests/test_pep8.py file.

### Dependencies

All dependencies needed for your package (that you may have installed via `pip install <dependency_name>`, for instance) should be specified in the `install_requires` list, e.g.,

    install_requires=['dependency1', 'dependency2', 'dependency3']
    
Specifying versions of your dependencies is recommended, but not required. If your package is intended for consumption as a library (never directly executed, only included as a dependency in other packages), loose or no version pins in setup.py are recommended. If your package is intended as a final application/main package (directly executed), strict version pins are recommended.

An example of no version pin:

    numpy

An example of a loose version pin:

    numpy>=1.11.0,<2.0.0
    
An example of a strict version pin:

    numpy==1.11.0

### Tests and test dependencies

The `tests_require` argument specifies the dependencies needed in order to run tests. Since test dependencies should
never be needed to use the package itself, the version numbers of test dependencies should always be strict, making
tests more repeatable. The `test_suite` argument specifies a different test package to use for finding and running
tests, if desired. A call to:

    python setup.py test
    
will install the needed dependencies to execute tests, then run the tests using the specified test suite.

This template uses nosetests for finding and running unit tests. Nose itself supports multiple plugins, such as
coverage calculation and reports. To utilize all of nose' plugins, it should be executed via either of the commands:

    nosetests
    python setup.py nosetests

Note: if nose tests are executed with the `--with-coverage` flag before any new modules are added, nose will
print an error regarding total coverage which can be safely ignored. This is a
[minor bug](https://github.com/nose-devs/nose/issues/1031) in the nose coverage plugin.

### Optional dependencies

The setup.py `extras_require` argument accepts a dictionary from a string to list of dependencies in the same format as
the `install_requires` or `tests_require` arguments. It is typical to specify a `tests` key in `extras_require`, so that
users may install test dependencies without running tests. This setup.py file also declares the needed dependencies
to generate API docs with Sphinx.

Optional dependencies are installed by adding brackets containing the extras keyword to a typical `pip install` command,
for example:

    pip install package_name[tests]
    pip install -e .[tests]

Optional dependencies should generally be truly *optional* for your package to function. Consequently, strict version
pins are usually appropriate to assist with repeatability. However, some packages place rarely-used dependencies into
optional dependencies, in order to reduce the minimum dependencies installed on all systems using the package (this
practice is not recommended by the author). In this case, follow the same conventions for specifying strict, loose, or
no version pins -- which depend on the intended consumption of your package.

### Main entry point

The setup.py `entry_points` argument with `console_scripts` dictionary entry registers an executable named `package_name` that will be available whenever your package is installed. This executable will run the code in the file package_name/\_\_main\_\_.py. `entry_points` also supports a `gui_scripts` argument for python applications that launch a GUI.

The \_\_main\_\_.py file contains a recommended template for a main method that will properly catch all raised exceptions and attempt to log them (annoyingly, exceptions are not logged by default in Python). If you don't use logging, you're welcome to replace the `try/except` block with any code desired. If you'd like to keep the exception handling in place, simply make a call in the `try` block to whatever code you'd like to execute (removing the call to `pass`). Consider using the [`argparse`](https://docs.python.org/2.7/library/argparse.html) library if you'd like to use command line arguments.

If no executable or main entry point is desired (usually the case in packages intended for consumption as libraries), the `entry_points` argument can simply be removed from setup.py and the \_\_main\_\_.py file can be safely deleted.

### setup.cfg

Additional configuration options typically used by setuptools plugins can be specified in setup.cfg. This package's
setup.cfg file provides additional options for Sphinx (API docs) and nose (unit tests) setuptools plugins:

1. `build_sphinx`: the location of the apidocs source code, where to output the built docs, and which files to include
1. `upload_sphinx`: the location of the apidocs directory that should be uploaded to a Python package repository such as
PyPi
1. `nosetests`: various test configuration settings, such as the location of the source code and coverage settings

If you don't intend to use nose or Sphinx, the relevant options can safely be removed from the setup.cfg file.

Semi-repeatable builds: requirements.txt
----------------

In addition to listing your dependencies in the `install_requires` argument in setup.py, you should also specify them in requirements.txt. Always use strict version pins in requirements.txt. Although setup.py can (and sometimes should) have loose or no version pins, requirements.txt is utilized to achieve repeatable builds. Consequently, all dependencies in requirements.txt should be explicitly version-pinned and all dependencies listed in setup.py should be listed in requirements.txt. This template explicitly pins numpy (`numpy==1.11.0`), which you are welcome to modify, if desired. New dependencies simply go on new lines, e.g.:

    dependency1==x.y.z
    dependency2==t.u.v

Add entries to the setup.py file and requirements.txt file only when you needed to explicitly install them. I.e., add only `dependency3` to your dependencies when you had to execute `pip install dependency3`, even if this installed more packages than just `dependency3`. pip will recursively install dependencies, so merely stating your dependency on `dependency3` is sufficient to install all the dependencies of `dependency3` as well.

Note that by not listing all installed dependencies in requirements.txt, requirements.txt does not provide fully-repeatable builds. Although fully repeatable builds are desirable, an overly-specific requirements.txt file can be incompatible with different operating systems, making cross-OS development and deployment quite challenging. If fully repeatable builds are desired, see the section *"Deploying your package"* for further details on how to guarantee full repeatability.

### Dependencies located in Git servers

If your package depends on other packages located in a Git server and not in PyPi or another Python package server, you can add them to the requirements.txt file exactly as they are specified in the `pip install` command for a VCS root. Namely, to depend on packages located in a Git server such as GitHub or bitbucket, add to your requirements.txt:

    git+https://<git-server-url>/<org-name>/<repo-name>.git@<branch-name>

Where `org-name` is the git server organization name, `repo-name` is the particular git repository, and `branch-name` is the name of the branch or tag corresponding to the particular version you want to install.

You can and should add the name of your dependency in the setup.py file, but `pip install` of your package will not be able to discover the locations of your dependencies in this case. The dependencies must first be installed from requirements.txt, then `pip install` will succeed. The template README below directs the user of your package to do this.

Note that if you spin up your own Python package server, such as [devpi-server](http://doc.devpi.net), this workaround is not required. In this scenario, you can easily depend on your own packages by uploading them to your package server, then installing them with simple `pip install <package_name>` commands, directing pip to use your Python package server instead of [PyPi](https://pypi.python.org/pypi).

Unit test example: package_name/tests/test_pep8.py
------------------------------
This is an example unit test that checks all of your source code for compliance with PEP8 style standards. It, and its
resource file located at package_name/tests/resources/pep8.cfg, can be safely removed, if desired.

API docs:
---------------

apidocs/conf.py is an example configuration file for use by Sphinx that uses a different Sphinx template than the
default. It also modifies the default Sphinx configuration to include documentation of `__init__` functions, which are
skipped by `sphinx-apidoc` by default.

The conf.py file can be deleted and will be recreated with Sphinx defaults by the `sphinx-apidoc` command discussed in
the section titled *"API Docs"*, if desired. Most of the `apidocs` subdirectory is excluded by `.gitignore`, since it
consists of files that are auto-generated by `sphinx-apidoc` and don't need to be committed.

To explicitly commit your conf.py file (if you want to use the conf.py file in this project, instead of the conf.py file
created by `sphinx-apidoc`), execute:

    git add -f apidocs/conf.py

Note that author and copyright in conf.py are currently set to 'author'. Feel free to modify for your package as appropriate.

If you don't wish to generate API docs or do not wish to use Sphinx, the apidocs directory can be safely removed. The
setup.cfg file should be edited to remove the `build_sphinx` and the `upload_sphinx` configuration options.

Deploying your package
--------------------
If you intend to deploy your package to a server, use `pip install` with the ["Only if needed" Recursive Upgrade pattern](https://pip.pypa.io/en/stable/user_guide/#only-if-needed-recursive-upgrade). This is the only safe mechanism by which to upgrade your package dependencies when strictly required without accidentally upgrading your dependencies when not required.

To ease development pain, this README recommends standards for the setup.py and requirements.txt files that do not guarantee fully repeatable builds. This is because Python dependencies are not OS-agnostic, making cross-OS development and deployment a challenge. In practice, the "Only if needed" Recursive upgrade pattern + testing first in stage are often sufficient for repeatable production deployment. However, using this pattern does *not* gaurantee repeatable production deployment. Notably, if the stage and production dependencies were installed the first time at significantly different times, package versions installed in stage will often not match production versions. Given that staging systems are frequently destroyed or may be initially deployed at very different times than the production system, it's easy for the package versions installed on stage to fall out of sync with the packages on production.

For fully repeatable builds, copy the Python package cache in production to stage, then use the "Only if needed" Recursive Upgrade pattern on stage, which exactly reproduces the deployment process that will be executed on production. To do so, use pip with the `--cache-dir` flag or use virtualenv. Other mechanisms for repeatable builds are to reverse the direction of copy (copy the cache from stage to prod; note you may end up with unnecessary test dependencies in production in this scenario), or to use `pip freeze` to generate a strict requirements.txt file on one system that can be used to install exactly matching dependencies on the other system. Naturally, all of these approaches assume that stage and production have identical operating sytems and architectures; they cannot be utilized if stage has a different OS or architecture than production.

Documentation you may wish to put in your own project
=====================================================
You may wish to retain the below content for use in your own python package. You may also want to consider linking to this template GitHub repository for instructions on getting the Python environment set up.

Installing For Use
------------------
If you'd simply like to use package_name, clone this repository, navigate into it, then execute inside your relevant virtualenv (if you have one):

    pip install -r requirements.txt
	pip install .

Note that this will not install the source code in your environment.

Installing For Development
--------------------------
If you want to develop this package, clone this repository, navigate into it, then create and use a virtualenv for this project:

    mkvirtualenv package_name
    pip install -r requirements.txt
    pip install -e .[tests]

This installs the `package_name` package, specific versions of its dependencies, and additional dependencies required for tests.

The additional `-e` flag configures the virtual environment to use the source code located in the `package_name` directory, which will cause code using the package to run against your latest changes to the source code without requiring you to reinstall the package every time you modify the code. Note: in most Python shells, once code is imported in a session, it's not reimported in the same session, even if you explicitly execute an import statement again. Consequently, if you change source code and want to use it in a running IPython notebook or Python shell that has already imported the code, you'll have to exit the notebook or shell in order to import your new source code.

Note that using `pip freeze` to create the requirements.txt file is discouraged, as this introduces dependencies that may not be system-agnostic. However, requirements.txt should pin the versions of every package listed in the `install_requires` section of setup.py.

Running Unit Tests
------------------
Unit tests are executed using nose. A setup.cfg is checked into the project specifying the nose configuration. Nose tests can be run from the command line or from IntelliJ/PyCharm.

### From the command line:

    nosetests -a '!disabled'

### From IntelliJ/PyCharm:

- Create a Run/Debug Configuration using the Python tests -> Nosetests template.
- Configure the Nosetests to run "All in folder", specifying the `package_name` directory as the folder.
- Set the params to: `-a '!disabled'`.
- Set the working directory to this top-level directory (one directory above the `package_name` directory).

### Code Coverage:

Running nosetests with the coverage flag enabled in IntelliJ/PyCharm [interferes with debugging](http://pydev.blogspot.com/2007/06/why-cant-pydev-debugger-work-with.html). Consequently, the with-coverage flag is not enabled in setup.cfg. Coverage can be determined from the command line by running `nosetests` with the `--with-coverage` flag. If using the nosetests `--with-coverage` flag, coverage reports in HTML with line highlighting can be read from the `cover/index.html` file.

Enterprise versions of IntelliJ/PyCharm can run coverage analysis on its own, but won't fail unit tests if coverage drops below the minimum set in setup.cfg.

API Docs
--------

If you would like the needed dependencies for generating API docs, execute:

    pip install -e .[docs]

If you wish to generate the API docs using Sphinx *including the source code* (see below), you **must** use `pip install -e .[docs]` to install this package, as the `-e` flag installs the source code in your environment for use by Sphinx. I.e., you cannot use `pip install .[docs]`.

The code is documented with reStructuredText syntax fed into Sphinx to create API docs. To generate API documentation:

    sphinx-apidoc -F -o apidocs package_name `find . \( -name tests -o -name resources \) -type d`
    python setup.py build_sphinx

This generates documentation for every package and module inside the package_name directory, except for directories named tests or resources. The HTML output will be available at build/html/index.html.

Committing
----------
Fork this repository and commit changes to your fork. When ready, submit a pull request for code review.

Unit tests for new functionality are strongly encouraged. All current tests must pass or be suitably modified.

Python dosctrings for all new functions and classes is highly recommended. Use reStructuredText syntax. Type hinting using [PyCharm's suggested type syntax](https://www.jetbrains.com/pycharm/webhelp/type-hinting-in-pycharm.html) is recommended.

If you update the python package version number in setup.py, update the package number in apidocs/conf.py as well.
